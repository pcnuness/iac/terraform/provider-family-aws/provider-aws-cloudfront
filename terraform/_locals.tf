locals {
  # Defina as variáveis locais aqui
  region      = var.region
  client_name = var.client_name
  environment = var.environment
  domain_name = var.domain_name

  cloudfront_price_class         = var.cloudfront_price_class
  cloudfront_default_root_object = var.cloudfront_default_root_object

  # Defina uma lista de mapas contendo os detalhes de cada CDN
  cdn_details = [
    {
      name           = "pcnuness-xp"
      custom_aliases = ["pcnuness-xp.${local.domain_name}"]
      s3_domain_name = "pcnuness-xp.s3.${local.region}.amazonaws.com"
    },
    {
      name           = "pcnuness-to"
      custom_aliases = ["pcnuness-to.${local.domain_name}"]
      s3_domain_name = "pcnuness-to.s3.${local.region}.amazonaws.com"
    }
  ]
}