## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.5.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | = 5.31 |
| <a name="requirement_local"></a> [local](#requirement\_local) | >= 2.4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.31.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_cloudfront"></a> [cloudfront](#module\_cloudfront) | terraform-aws-modules/cloudfront/aws | ~> 3.2 |
| <a name="module_s3_bucket"></a> [s3\_bucket](#module\_s3\_bucket) | terraform-aws-modules/s3-bucket/aws | ~> 3.15 |

## Resources

| Name | Type |
|------|------|
| [aws_s3_bucket_policy.bucket_policy](https://registry.terraform.io/providers/hashicorp/aws/5.31/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_policy.this](https://registry.terraform.io/providers/hashicorp/aws/5.31/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_object.this](https://registry.terraform.io/providers/hashicorp/aws/5.31/docs/resources/s3_object) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/5.31/docs/data-sources/caller_identity) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/5.31/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_client_name"></a> [client\_name](#input\_client\_name) | Custome Name | `string` | `"pcnuness"` | no |
| <a name="input_cloudfront_default_root_object"></a> [cloudfront\_default\_root\_object](#input\_cloudfront\_default\_root\_object) | Default root object for cloudfront. Need to also provide custom error response if changing from default | `string` | `"index.html"` | no |
| <a name="input_cloudfront_default_ttl"></a> [cloudfront\_default\_ttl](#input\_cloudfront\_default\_ttl) | The default TTL for the cloudfront cache | `number` | `86400` | no |
| <a name="input_cloudfront_max_ttl"></a> [cloudfront\_max\_ttl](#input\_cloudfront\_max\_ttl) | The maximum TTL for the cloudfront cache | `number` | `31536000` | no |
| <a name="input_cloudfront_min_ttl"></a> [cloudfront\_min\_ttl](#input\_cloudfront\_min\_ttl) | The minimum TTL for the cloudfront cache | `number` | `0` | no |
| <a name="input_cloudfront_minimum_protocol_version"></a> [cloudfront\_minimum\_protocol\_version](#input\_cloudfront\_minimum\_protocol\_version) | The minimum version of the SSL protocol that you want CloudFront to use for HTTPS connections. | `string` | `"TLSv1.2_2019"` | no |
| <a name="input_cloudfront_price_class"></a> [cloudfront\_price\_class](#input\_cloudfront\_price\_class) | CloudFront distribution price class | `string` | `"PriceClass_100"` | no |
| <a name="input_domain_name"></a> [domain\_name](#input\_domain\_name) | Domain name for website, used for all resources | `string` | `"pcnunes.com.br"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | AWS Landing Zone | `string` | `"lab"` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS Landing Zone | `string` | `"us-east-1"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cloudfront_endpoints"></a> [cloudfront\_endpoints](#output\_cloudfront\_endpoints) | n/a |

## InfraCost

```
 Name                                                                        Monthly Qty  Unit                    Monthly Cost 
                                                                                                                               
 module.cloudfront["pcnuness-to"].aws_cloudfront_distribution.this[0]                                                          
 ├─ Invalidation requests (first 1k)                                   Monthly cost depends on usage: $0.00 per paths          
 └─ US, Mexico, Canada                                                                                                         
    ├─ Data transfer out to internet (first 10TB)                      Monthly cost depends on usage: $0.085 per GB            
    ├─ Data transfer out to origin                                     Monthly cost depends on usage: $0.02 per GB             
    ├─ HTTP requests                                                   Monthly cost depends on usage: $0.0075 per 10k requests 
    └─ HTTPS requests                                                  Monthly cost depends on usage: $0.01 per 10k requests   
                                                                                                                               
 module.cloudfront["pcnuness-xp"].aws_cloudfront_distribution.this[0]                                                          
 ├─ Invalidation requests (first 1k)                                   Monthly cost depends on usage: $0.00 per paths          
 └─ US, Mexico, Canada                                                                                                         
    ├─ Data transfer out to internet (first 10TB)                      Monthly cost depends on usage: $0.085 per GB            
    ├─ Data transfer out to origin                                     Monthly cost depends on usage: $0.02 per GB             
    ├─ HTTP requests                                                   Monthly cost depends on usage: $0.0075 per 10k requests 
    └─ HTTPS requests                                                  Monthly cost depends on usage: $0.01 per 10k requests   
                                                                                                                               
 module.s3_bucket["pcnuness-to"].aws_s3_bucket.this[0]                                                                         
 └─ Standard                                                                                                                   
    ├─ Storage                                                         Monthly cost depends on usage: $0.023 per GB            
    ├─ PUT, COPY, POST, LIST requests                                  Monthly cost depends on usage: $0.005 per 1k requests   
    ├─ GET, SELECT, and all other requests                             Monthly cost depends on usage: $0.0004 per 1k requests  
    ├─ Select data scanned                                             Monthly cost depends on usage: $0.002 per GB            
    └─ Select data returned                                            Monthly cost depends on usage: $0.0007 per GB           
                                                                                                                               
 module.s3_bucket["pcnuness-xp"].aws_s3_bucket.this[0]                                                                         
 └─ Standard                                                                                                                   
    ├─ Storage                                                         Monthly cost depends on usage: $0.023 per GB            
    ├─ PUT, COPY, POST, LIST requests                                  Monthly cost depends on usage: $0.005 per 1k requests   
    ├─ GET, SELECT, and all other requests                             Monthly cost depends on usage: $0.0004 per 1k requests  
    ├─ Select data scanned                                             Monthly cost depends on usage: $0.002 per GB            
    └─ Select data returned                                            Monthly cost depends on usage: $0.0007 per GB           
                                                                                                                               
 OVERALL TOTAL                                                                                                           $0.00 
──────────────────────────────────
22 cloud resources were detected:
∙ 4 were estimated, all of which include usage-based costs, see https://infracost.io/usage-file
∙ 14 were free:
  ∙ 4 x aws_s3_bucket_policy
  ∙ 2 x aws_cloudfront_origin_access_identity
  ∙ 2 x aws_s3_bucket_acl
  ∙ 2 x aws_s3_bucket_ownership_controls
  ∙ 2 x aws_s3_bucket_public_access_block
  ∙ 2 x aws_s3_bucket_versioning
∙ 4 are not supported yet, see https://infracost.io/requested-resources:
  ∙ 2 x aws_cloudfront_origin_access_control
  ∙ 2 x aws_s3_object

┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━┓
┃ Project                                                          ┃ Monthly cost ┃
┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━┫
┃ pcnuness/iac/terraform/provider...vider-aws-cloudfront/terraform ┃ $0.00        ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┻━━━━━━━━━━━━━━┛
```