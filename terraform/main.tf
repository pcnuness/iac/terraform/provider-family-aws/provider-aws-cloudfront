module "s3_bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "~> 3.15"

  for_each = { for cdn in local.cdn_details : cdn.name => cdn }

  bucket                   = each.value.name
  acl                      = "private"
  control_object_ownership = true
  object_ownership         = "ObjectWriter"
  force_destroy            = false
  versioning = {
    status     = true
    mfa_delete = false
  }
}

resource "aws_s3_object" "this" {
  for_each = { for cdn in local.cdn_details : cdn.name => cdn }

  bucket       = each.value.name
  key          = "index.html"
  source       = "${path.module}/../src/index.html"
  content_type = "text/html"
  etag         = filemd5("${path.module}/../src/index.html")

  depends_on = [
    module.s3_bucket
  ]
}

module "cloudfront" {
  source  = "terraform-aws-modules/cloudfront/aws"
  version = "~> 3.2"

  for_each = { for cdn in local.cdn_details : cdn.name => cdn }

  #aliases             = concat([replace("${each.key}.${local.domain_name}", "-${local.environment}", "")], each.value.custom_aliases)
  comment             = "CDN ${local.client_name} - ${each.value.custom_aliases[0]}"
  wait_for_deployment = false
  enabled             = true
  is_ipv6_enabled     = false
  retain_on_delete    = false
  price_class         = local.cloudfront_price_class
  default_root_object = local.cloudfront_default_root_object
  http_version        = "http2"

  create_monitoring_subscription = false
  create_origin_access_identity  = true
  origin_access_identities = {
    s3_bucket_one = each.value.s3_domain_name
  }

  create_origin_access_control = true
  origin_access_control = {
    "${each.value.s3_domain_name}" = {
      description      = "CDN OAC ${each.value.s3_domain_name}"
      origin_type      = "s3"
      signing_behavior = "always"
      signing_protocol = "sigv4"
    }
  }

  origin = {
    "${each.value.s3_domain_name}" = {
      origin_access_control = "${each.value.s3_domain_name}"
      domain_name           = "${each.value.s3_domain_name}"
      custom_origin_config = {
        http_port              = 80
        https_port             = 443
        origin_protocol_policy = "match-viewer"
        origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
      }

    }
    "${each.value.s3_domain_name}" = {
      domain_name           = "${each.value.s3_domain_name}"
      origin_access_control = "${each.value.s3_domain_name}"
    }
  }

  default_cache_behavior = {
    path_pattern           = "*"
    target_origin_id       = "${each.value.s3_domain_name}"
    viewer_protocol_policy = "redirect-to-https"
    // response_headers_policy_id = aws_cloudfront_response_headers_policy.security.id
    allowed_methods = ["GET", "HEAD", "OPTIONS"]
    cached_methods  = ["GET", "HEAD"]
    compress        = true
    query_string    = true
  }

  viewer_certificate = {
    cloudfront_default_certificate = true
  }

  depends_on = [
    module.s3_bucket
  ]
}

resource "aws_s3_bucket_policy" "this" {
  for_each = { for cdn in local.cdn_details : cdn.name => cdn }

  bucket = each.key

  policy = jsonencode({
    "Version" : "2008-10-17",
    "Id" : "PolicyForCloudFrontPrivateContent",
    "Statement" : [
      {
        "Sid" : "AllowCloudFrontServicePrincipal",
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "cloudfront.amazonaws.com"
        },
        "Action" : "s3:GetObject",
        "Resource" : "${module.s3_bucket["${each.key}"].s3_bucket_arn}/*"
        "Condition" : {
          "StringEquals" : {
            "AWS:SourceArn" : "${module.cloudfront["${each.key}"].cloudfront_distribution_arn}"
          }
        }
      }
    ]
  })

  depends_on = [
    module.cloudfront
  ]

}

resource "aws_s3_bucket_policy" "bucket_policy" {
  for_each = { for cdn in local.cdn_details : cdn.name => cdn }

  bucket = module.s3_bucket["${each.value.name}"].s3_bucket_id
  policy = aws_s3_bucket_policy.this["${each.value.name}"].policy
}