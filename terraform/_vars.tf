variable "region" {
  default     = "us-east-1"
  description = "AWS Landing Zone"
}

variable "client_name" {
  default     = "pcnuness"
  description = "Custome Name"
}

variable "environment" {
  default     = "lab"
  description = "AWS Landing Zone"
}

variable "domain_name" {
  type        = string
  default     = "pcnunes.com.br"
  description = "Domain name for website, used for all resources"
}

variable "cloudfront_price_class" {
  type        = string
  default     = "PriceClass_100" # Only US,Canada,Europe
  description = "CloudFront distribution price class"
}
variable "cloudfront_default_root_object" {
  type        = string
  default     = "index.html"
  description = "Default root object for cloudfront. Need to also provide custom error response if changing from default"
}

variable "cloudfront_min_ttl" {
  type        = number
  default     = 0
  description = "The minimum TTL for the cloudfront cache"
}

variable "cloudfront_default_ttl" {
  type        = number
  default     = 86400
  description = "The default TTL for the cloudfront cache"
}

variable "cloudfront_max_ttl" {
  type        = number
  default     = 31536000
  description = "The maximum TTL for the cloudfront cache"
}

variable "cloudfront_minimum_protocol_version" {
  type        = string
  description = "The minimum version of the SSL protocol that you want CloudFront to use for HTTPS connections."
  default     = "TLSv1.2_2019"
}
