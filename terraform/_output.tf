output "cloudfront_endpoints" {
  value = {
    for cdn in local.cdn_details :
    cdn.custom_aliases[0] => module.cloudfront[cdn.name].cloudfront_distribution_domain_name
  }
}