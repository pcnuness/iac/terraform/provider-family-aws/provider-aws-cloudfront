terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 5.31"
    }

    local = {
      source  = "hashicorp/local"
      version = ">= 2.4.0"
    }
  }

  required_version = "1.5.7"
}